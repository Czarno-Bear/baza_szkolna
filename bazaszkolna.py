import sys

# Inicjacja tablicy 'school_base' oraz zmiennej index = input().
# Wczytanie z pliku in.txt danych do tablicy school_base. Jeśli wczytane
# to nastepuje break. Na koncu dodanie do tablicy slowa 'koniec'
# ---------------------------------------------------------------------------

school_base = []
index = input()

while True:
    if index != 'koniec':
        school_base.append(index)
        index = input()
    else:
        break
school_base.append('koniec')

# -----------------------------------------------------------------------------
# Inicjacja slownikow 'student, teacher, educator'
# oraz list 'student_list, add_student_list, subject'
# -----------------------------------------------------------------------------

student = {}
teacher = {}
educator = {}

student_list = []
add_student_list = []
subject = []

# -----------------------------------------------------------------------------
# Pobieranie w pętli konkretnych wartosci i umieszczanie ich w slownikach
# oraz tablicach. Jako klucz w slowniku ustawianie nazwy klasy np. '1c, 2b'
# itp.
# -----------------------------------------------------------------------------

i = 0
while school_base:

    # -------------------------------------------------------------------------
    # Jesli 'wychowawca' to... Slownik w ktorym value ustawienie jego
    # imienia i nazwiska a jako klucz ustawienie klasy ktorej jest
    # wychowawca
    # -------------------------------------------------------------------------

    if school_base[i] == 'wychowawca':
        value_educator = school_base[i + 1]
        i += 1
        meter = len(school_base[i])
        idx = i

        # ---------------------------------------------------------------------
        # Zmienna meter liczy ilosc liter w pobranym wierszu.
        # nastepnie ustawienie klucza oraz wartosci w slowniku
        # ---------------------------------------------------------------------

        while True:
            idx += 1
            meter = len(school_base[idx])
            if meter == 2:
                key_educator = school_base[idx]
                educator[key_educator] = value_educator
            elif meter == 1 or meter > 2:
                continue
            else:
                break

    # -------------------------------------------------------------------------
    # Dzialanie jak powyzsze z tym ze jako wartosc pobrana jest tablica
    # z imionami nauczycieli oraz przedmiotami ktorych ucza.
    # kluczem jak wyzej jest nazwa klasy
    # -------------------------------------------------------------------------

    elif school_base[i] == 'nauczyciel':

        value_teacher = school_base[i + 1]
        value_subject = school_base[i + 2]

        meter = len(school_base[i])
        idx = i
        while meter != 0:
            idx += 1
            meter = len(school_base[idx])
            if meter == 2:
                key = school_base[idx]
                if teacher == {}:
                    key_teacher = school_base[idx]
                    subject.append(value_teacher)
                    subject.append(value_subject)
                    teacher[key_teacher] = subject
                elif key not in teacher:
                    key_teacher = school_base[idx]
                    subject.append(value_teacher)
                    subject.append(value_subject)
                    teacher[key_teacher] = subject
                elif key in teacher:
                    subject = list(teacher[key])
                    subject.append(value_teacher)
                    subject.append(value_subject)
                    del teacher[key]
                    teacher[key] = subject
            elif meter == 1 or meter > 2:
                continue
            else:
                break
            subject = []

    # -------------------------------------------------------------------------
    # podobnie jak wyzej. Kluczem jest nazwa klasy a jako wartosc tablica
    # z uczniami ktorzy sa w tej klasie
    # -------------------------------------------------------------------------

    elif school_base[i] == 'uczen':

        key = school_base[i + 2]
        value = school_base[i + 1]
        if student == {}:
            student_list.append(value)
            student = {key: student_list}
        elif key in student:
            v = list(student[key])
            v.append(value)
            del student[key]
            student[key] = v
        else:
            add_student_list.append(value)
            student[key] = add_student_list
            add_student_list = []

    # -------------------------------------------------------------------------
    # Zakonczenie petli jesli koniec
    # -------------------------------------------------------------------------
    elif school_base[i] == 'koniec':
        break
    elif school_base[i] == '':
        i += 1
        continue
    i += 1

# -----------------------------------------------------------------------------
# Mozna sobie wyswietlic poszczegolne slowniki z wynikami

# print(student)
# print('-' * 30)
# print(teacher)
# print('-' * 30)
# print(educator)
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Pobranie wartosci z terminala
# -----------------------------------------------------------------------------

action = sys.argv[1]

# -----------------------------------------------------------------------------
# Jesli 'nazwa klasy', wyswietlenie wychowawcy i uczniow w klasie
# -----------------------------------------------------------------------------

while True:
    if student.get(action):
        print(educator[action])
        print(student[action])
        break
    else:
        break

# -----------------------------------------------------------------------------
# Jesli 'wychowawca', wypisanie wszystkich jego uczniow
# -----------------------------------------------------------------------------

for k, v in educator.items():
    if v != action:
        continue
    elif v == action:
        print(k)
        print(student[k])
    else:
        break

# -----------------------------------------------------------------------------
# Jesli 'nauczyciel' wypisanie wychowawcow z klas ktore on uczy

for k, v in teacher.items():
    if v:
        i = 0

        while i < len(v):
            if v[i] == action:
                print([k])
                if educator.get(k) is None:    # obsluga bledu jesli brak
                    print('Brak wychowawcy')   # wychowawcy
                    break
                else:
                    print(educator[k])
            i += 1
    else:
        break

# -----------------------------------------------------------------------------
# Jesli 'uczen' to wypisanie jego nauczycieli i przedmiotow ktorych ucza
# -----------------------------------------------------------------------------

for k, v in student.items():
    if v:
        i = 0
        while i < len(v):
            if v[i] == action:
                print(v[i])
                print(teacher[k])

            i += 1
    else:
        break
